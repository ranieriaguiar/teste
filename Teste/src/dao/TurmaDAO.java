package dao;

import java.util.ArrayList;
import java.util.List;

import model.Aluno;
import model.Turma;

public class TurmaDAO {

	private List<Turma> turmaRepositorio;

	public TurmaDAO() {
		turmaRepositorio = new ArrayList<>();
	}

	public void inserirTurma(Turma t) {
		turmaRepositorio.add(t);
	}

	public Turma buscarPorNome(Turma t) {
		for (Turma turma : turmaRepositorio) {
			if (turma.getNome() == t.getNome()) {
				return turma;
			}
		}
		return null;
	}

	public List<Aluno> listaAlunosDaTurma(Turma t) {
		for (Turma turma : turmaRepositorio) {
			if (turma.getCodigo() == t.getCodigo()) {
				return turma.getListaAlunos();
			}
		}
		return null;
	}
}
