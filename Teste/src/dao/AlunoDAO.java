package dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import model.Aluno;

public class AlunoDAO {

	private List<Aluno> alunoRepositorio;
	private int idAutoIncrement;

	public AlunoDAO() {
		alunoRepositorio = new ArrayList<>();
		idAutoIncrement = 1;
	}

	public void inserirAluno(Aluno a) {
		a.setId(idAutoIncrement);
		alunoRepositorio.add(a);
		idAutoIncrement++;
	}

	public void alterarAluno(Aluno a) {
		for (Aluno aluno : alunoRepositorio) {
			if (aluno.getId() == a.getId()) {
				aluno = a;
				break;
			}
		}
	}

	public Aluno buscarPorNome(Aluno a) {
		for (Aluno aluno : alunoRepositorio) {
			if (aluno.getNome() == a.getNome()) {
				return aluno;
			}
		}
		return null;
	}

	public Aluno buscarPorNomeMae(Aluno a) {
		for (Aluno aluno : alunoRepositorio) {
			if (aluno.getNomeMae() == a.getNomeMae()) {
				return aluno;
			}
		}
		return null;
	}

	public List<Aluno> listaAluno(float mediaMaxima) {
		List<Aluno> alunosAbaixoMedia = new ArrayList<>();

		for (Aluno aluno : alunoRepositorio) {
			if (aluno.getMedia() < mediaMaxima) {
				alunosAbaixoMedia.add(aluno);
			}

		}

		if (!alunosAbaixoMedia.isEmpty()) {
			Collections.sort(alunosAbaixoMedia, Comparator.comparing(Aluno::getNome));
		}
		return alunosAbaixoMedia;
	}

	public List<Aluno> listaAluno() {
		Collections.sort(alunoRepositorio, Comparator.comparing(Aluno::getNome));
		return alunoRepositorio;
	}
}
