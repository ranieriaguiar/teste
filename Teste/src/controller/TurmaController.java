package controller;

import java.util.List;

import dao.TurmaDAO;
import model.Turma;
import model.Aluno;

public class TurmaController {

	private TurmaDAO dao;

	public TurmaController() {
		dao = new TurmaDAO();
	}

	public void salvarTurma(Turma t) {
		boolean erro = false;
		if (dao.buscarPorNome(t) != null) {
			erro = true;
		}
		if (!erro) {
			dao.inserirTurma(t);
		}
	}

	public List<Aluno> listarAlunoPorTurma(Turma t) {
		return dao.listaAlunosDaTurma(t);
	}

}
