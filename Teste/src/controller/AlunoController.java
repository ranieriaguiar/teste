package controller;

import java.util.List;

import dao.AlunoDAO;
import model.Aluno;

public class AlunoController {

	private AlunoDAO dao;

	public AlunoController() {
		dao = new AlunoDAO();
	}

	public void salvarAluno(Aluno a) {
		boolean erro = false;

		if (dao.buscarPorNome(a) != null) {
			erro = true;
		}
		if (dao.buscarPorNomeMae(a) != null) {
			erro = true;
		}
		if (!erro) {
			dao.inserirAluno(a);
		}
	}

	public void atualizarAluno(Aluno a) {
		boolean erro = false;
		Aluno aluno;

		aluno = dao.buscarPorNome(a);
		if (aluno != null && aluno.getId() != a.getId()) {
			erro = true;
		}

		aluno = dao.buscarPorNomeMae(a);
		if (aluno != null && aluno.getId() != a.getId()) {
			erro = true;
		}

		if (!erro) {
			dao.alterarAluno(a);
			if (a.getMedia() < 5) {
				System.out.println(a.getNome());
			}
		}
	}

	public List<Aluno> listarAluno() {
		return dao.listaAluno();
	}

	public List<Aluno> listarAluno(float mediaMaxima) {
		return dao.listaAluno(5);
	}
}
